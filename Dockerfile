# syntax=docker/dockerfile:1

# Utiliser une image de node avec la version souhaitée
FROM node:latest
# Mettre en place le répertoire de travail
WORKDIR /app
# Copier les fichiers du projet Angular dans le conteneur
COPY . .
# Installer Angular CLI globalement
RUN npm install -g @angular/cli
# Installer les dépendances du projet
RUN npm install
# Commande pour lancer le serveur Angular avec un host accessible de l'extérieur
CMD ["ng","serve", "--host", "0.0.0.0"]
# Exposer le port 4200 utilisé par l'application Angular
EXPOSE 4200
